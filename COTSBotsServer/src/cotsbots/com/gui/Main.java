package cotsbots.com.gui;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import cotsbots.com.server.ClientThread;
import cotsbots.com.server.Robot;
import cotsbots.com.server.Server;

import cotsbots.robot.data.EvolutionConditions;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.List;
import java.util.ArrayList;

public class Main extends JFrame {

	private JPanel contentPane;
	private Server myServer;
	public static Main frame;
	JLabel lblIpAddress = new JLabel("IP Address:");
	JLabel lblIP = new JLabel("Discconected");
	JLabel lblStatus = new JLabel("Status");
	JLabel lblServerStatus = new JLabel("Server Status");
	JButton btnStartServer = new JButton("Start Server");
	JButton btnDisconnect = new JButton("Disconnect");
	JLabel lblRobots = new JLabel("Robots");
	List lstRobots = new List();
	Boolean updated = new Boolean(false);
	
	ArrayList<ClientThread> robots = new ArrayList<ClientThread>();
	
	private final JButton btnControlRobot = new JButton("Control Robot");
	private final JButton btnSetupEvolution = new JButton("Setup Evolution");
	private final JButton btnViewRobotData = new JButton("View Robot Data");
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 514, 379);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//frame.setTitle("A new Server");
		
		myServer = new Server(10000, lblServerStatus, lblIP,  lstRobots );
		robots = myServer.getRobots();
		setupGUI();
		
		
		
		
	}
	@SuppressWarnings("deprecation")
	public void setupGUI(){
		
		lblIpAddress.setBounds(10, 238, 63, 14);
		contentPane.add(lblIpAddress);
		
		
		lblIP.setBounds(104, 238, 75, 14);
		contentPane.add(lblIP);
		
		
		lblStatus.setBounds(260, 208, 46, 14);
		contentPane.add(lblStatus);
		
		
		lblServerStatus.setBounds(257, 238, 151, 14);
		contentPane.add(lblServerStatus);
		
		
		btnStartServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new runServer().start();
				lblIP.setText(myServer.IPaddress);
				
			}
		});
		btnStartServer.setBounds(10, 204, 89, 23);
		contentPane.add(btnStartServer);
		
		
		lblRobots.setBounds(10, 11, 46, 14);
		contentPane.add(lblRobots);
		
		
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				myServer.disconnect();
			}
		});
		btnDisconnect.setBounds(119, 204, 89, 23);
		contentPane.add(btnDisconnect);
		
		
		btnControlRobot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RobotControl RC = new RobotControl(robots);
				RC.setVisible(true);
				
			}
		});
		btnControlRobot.setBounds(10, 109, 99, 23);
		contentPane.add(btnControlRobot);
		btnSetupEvolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EvolutionConditions c = new EvolutionConditions();
				EvolveGroup EG = new EvolveGroup(c, robots.size(), robots, lstRobots);
				EG.setVisible(true);
			}
		});
		btnSetupEvolution.setBounds(224, 33, 145, 23);
		
		contentPane.add(btnSetupEvolution);
		
		
		lstRobots.setMultipleSelections(false);
		lstRobots.setBounds(10, 31, 198, 60);
		contentPane.add(lstRobots);
		btnViewRobotData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RobotDataView rdv = new RobotDataView(robots);
				rdv.setVisible(true);
			}
		});
		btnViewRobotData.setBounds(119, 109, 89, 23);
		
		contentPane.add(btnViewRobotData);
		//listtest.add
		
	}
	
	
	
	class runServer extends Thread{
		public void run(){
			myServer.start();
		}
	}
}
