package cotsbots.com.server;

import cotsbots.robot.data.RobotData;


public class Robot {
	public int ID;
	public RobotData data;
	
	public Robot( int robotID){

		ID = robotID;
		data = new RobotData();
	}
}
