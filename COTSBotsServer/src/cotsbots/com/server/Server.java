package cotsbots.com.server;



import java.net.*;
import java.util.*;

import javax.swing.JLabel;

public class Server {
	
	
	
	private static int uniqueID = 0;
	private ArrayList<ClientThread> clients = new ArrayList<ClientThread>();
	private int port;
	private boolean running;
	public String IPaddress;
	public statusUpdater status;
	public ArrayList<Robot> robots;
	public java.awt.List info;

	
	
	
	public Server(int portNum, JLabel stat, JLabel ip,java.awt.List lstRobots){
		port = portNum;
		status = new statusUpdater(stat, ip);
		status.update("Started");
		info = lstRobots;
		status.start();
		
		
		
	}
	public ArrayList<ClientThread> getRobots(){
		return clients;
	}
	
	public void start(){
		running = true;
		status.update("Trying");
		try{
			ServerSocket myServerSocket = new ServerSocket(port);
			IPaddress = myServerSocket.getInetAddress().getHostAddress();
			status.update("Connected");
			status.updateIP(IPaddress);
			while(running){
				info.add("Running");
				Socket mySocket = myServerSocket.accept();
					if(!running){
						break;
					}
				info.add("Robot " + uniqueID + " Connected");
				
				ClientThread t = new ClientThread(mySocket, uniqueID, info);
				info.add("Robot " + t.ID + " CT made");
				uniqueID++;
				clients.add(t);
				info.add("Robot " + t.ID + " CT added");
				t.start();
				info.add("Robot " + t.ID + " CT stared");
				
				//info.add("End");
				
				
			}
			
			try{
				myServerSocket.close();
				for(ClientThread client:clients){
					try{
						client.input.close();
						client.output.close();
						client.socket.close();
					}catch(Exception e){
						
					}
				}
			}catch(Exception e){
				
			}
		}catch(Exception e){
			
		}
		running = true;
	}
	public void disconnect(){
		running = false;
		status.update("Disconnected");
		try{
			new Socket("localhost", port);
		}catch(Exception e){
			
		}
	}
	
	
	
	public class statusUpdater extends Thread{
		public String st = "", ipaddress = "";
		public JLabel label, ip;
	
		statusUpdater(JLabel l, JLabel i){
			label = l;
			ip = i;
		}
		public void update(String stat){
			st = stat;
		}
		public void updateIP(String ipa){
			ipaddress = ipa;
		}
		public void run(){
			while(true){
				label.setText(st);
				ip.setText(ipaddress);
			}
		}
	}
	
}

