package ui.lair.roadClassification;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import ui.lair.imaging.ImageProcessor;
import ui.lair.imaging.Model;
import ui.lair.imaging.RoadModel;

import android.util.Log;


public class RGB_HSV_Classifier extends RoadClassifier implements IHistogramModel{
	
	private List< RoadModel > roadModels = new ArrayList< RoadModel>();
	
	public RGB_HSV_Classifier(int sX, int sY, int eX, int eY){
		super(ImageProcessor.MODEL_TYPE_RGB, sX, sY, eX, eY);
	}
	
// IModel implementations

	@Override
	public void updateModel(Mat cameraImage){
		if(isBuildingLibrary()){
			Mat roadRegion = new Mat();
			roadRegion = handleSubRegion(cameraImage);
			
			
			addModel(roadRegion);
		}
	}
	
	@Override
	public int getNumberOfModels(){
		
		return roadModels.size();
	}

	@Override
	public double calculateRoadProbability(Mat region) {
		
		return 0;
	}
	
	@Override
	public Mat getModelHistogram(int index1, int index2) {
		return roadModels.get(index1).getModel(index2).getHistogram();
	}
	
	@Override 
	public void deleteModels(){
		roadModels.clear();
	}
	
	@Override
	public Mat handleSubRegion( Mat input1 ) {
		Mat tex1 = new Mat();
		Mat input2 = new Mat();
		Mat output1 = new Mat();
		Mat output2 = new Mat();
		
		Imgproc.cvtColor(input1, tex1, Imgproc.COLOR_RGB2YCrCb);
		List<Mat> channels = ImageProcessor.split3Channels(tex1);
		ImageProcessor.equalizeAndMerge(channels, input1);
		
		Imgproc.cvtColor(input1, input2, Imgproc.COLOR_RGB2HSV);
		
		output1 = input1.submat(startY, endY, startX, endX);
		output2 = input2.submat(startY, endY, startX, endX);
		
		List<Mat> histograms = new ArrayList<Mat>();
		histograms.add(output1);
		histograms.add(output2);
		
		Mat output = new Mat();
		Core.vconcat(histograms, output);
		
		return output;
	}
	
	@Override
	public void addModel(Mat newHistogram){
		
		Model newModel = new Model(newHistogram);
		
		// if no current models then add the new model immediately
		if(roadModels.size() == 0){
			addNewRoadModel(newModel);
		}
		else if(roadModels.size() <= MAX_MODELS){
			int intBestRoadModelMatchIndex = -1;
			int intBestColorModelMatchIndex = -1;
			double dblBestMatchValue = -1;
			
			
			// For each road model
			for(int i = 0; i < roadModels.size(); i++){
				
				//compare all examples of the model to the new one
				for(int j = 0; j < roadModels.get(i).getSize(); j++){
					roadModels.get(i).getModel(j).incrementAge();
					double dblHistogramSimilarity = calculateHistogramSimilarity(roadModels.get(i).getModel(j), newHistogram);
					
					
					
					if(dblHistogramSimilarity >= ROAD_MODEL_SIMILARITY_THRESHOLD && dblHistogramSimilarity > dblBestMatchValue){
						intBestRoadModelMatchIndex = i;
						intBestColorModelMatchIndex = j;
						dblBestMatchValue = dblHistogramSimilarity;
					}
				}
			}
			
			// if a similar one is found, update the road model
			if(intBestRoadModelMatchIndex > -1 && intBestColorModelMatchIndex > -1){
				updateModelExmaple(intBestRoadModelMatchIndex, intBestColorModelMatchIndex, dblBestMatchValue, newModel);
			}
			else{ // add new road model
				addNewRoadModel(newModel);
			}
		}
			
	
		
	}
	
	
	public void updateModelExmaple(int intBestRoadModelMatchIndex, int intBestColorModelMatchIndex, 
			 double dblBestMatchValue, Model newModel){
//roadModels.get(intBestRoadModelMatchIndex).set(intBestColorModelMatchIndex, newColorModel);
RoadModel roadModel = roadModels.get(intBestRoadModelMatchIndex);
roadModel.getModel(intBestColorModelMatchIndex).incrementNumberOfSelections();

if(dblBestMatchValue < COLOR_MODEL_SIMILARITY_THRESHOLD){
//if room in the road model just add the new color model
if(roadModel.getSize() < MAX_MODELS){
roadModel.addModel(newModel);
}
else{ //remove the lease selected color model and add the new one
double dblLowestSelectionPercentage = Double.MAX_VALUE;
int intFewestSelectionsIndex = -1;

for(int i = 0; i < roadModel.getSize(); i++){
if(roadModel.getModel(i).getSelectionPercentage() < dblLowestSelectionPercentage){
	intFewestSelectionsIndex = i;
	dblLowestSelectionPercentage = roadModel.getModel(i).getSelectionPercentage();
}
}


roadModel.replaceModel(intFewestSelectionsIndex, newModel);

Log.i(this.getClass().getSimpleName(), "Removed color model " + intFewestSelectionsIndex + 
	" from road model " + intBestRoadModelMatchIndex);
}
}
}
		
	public void addNewRoadModel(Model newModel){
		if(roadModels.size() < MAX_MODELS){
			roadModels.add(new RoadModel(newModel));
		}
		else if(roadModels.size() == MAX_MODELS){
			double dblLowestSelectionPercentage = Double.MAX_VALUE;
			int intLowestSelectionsIndex = -1;
			
			for(int i = 0; i < roadModels.size(); i++){
				if(roadModels.get(i).getSelectionPercentage() < dblLowestSelectionPercentage){
					dblLowestSelectionPercentage = roadModels.get(i).getSelectionPercentage();
					intLowestSelectionsIndex = i;
				}
			}
			
			roadModels.remove(intLowestSelectionsIndex);
			roadModels.add(new RoadModel(newModel));
		}
	}

}
