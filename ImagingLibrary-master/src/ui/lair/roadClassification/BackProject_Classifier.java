package ui.lair.roadClassification;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import ui.lair.imaging.ImageProcessor;

import android.util.Log;


public class BackProject_Classifier extends RoadClassifier {
	private List<Mat> roadExamples = new ArrayList<Mat>();

	public BackProject_Classifier(int sX, int sY, int eX, int eY){
		super(ImageProcessor.MODEL_TYPE_BACKPROJECT, sX, sY, eX, eY);
	}

	// IModel implementations

	
	
	@Override
	public void updateModel(Mat cameraImage){
		if(isBuildingLibrary()){
			Mat roadRegion = new Mat();
			roadRegion = handleSubRegion(cameraImage);
			Mat hist = ImageProcessor.caclulateHistogram(ImageProcessor.RGB, roadRegion, null);
			
			
			Log.e(this.getClass().getSimpleName(), hist.channels()+"");
			//Log.e(this.getClass().getSimpleName(), hist.rows() + "");
			
			roadExamples.add(hist);
		}
	}
	
	@Override
	public int getNumberOfModels(){
		
		return roadExamples.size();
	}

	@Override
	public double calculateRoadProbability(Mat region) {
		
		return 0;
	}
	
	@Override
	public Mat getModelHistogram(int index1, int index2) {
		return roadExamples.get(index1);
	}
	
	@Override
	public void deleteModels(){
		roadExamples.clear();
	}
	
	@Override
	public Mat handleSubRegion( Mat input1) {
		return input1.submat(startY, endY, startX, endX);
	}
	
	public double[] calc(Mat m_Rgba, int numCols, int numRows){
		
		int numInput = numCols*numRows;
		double[] outputProbabilities = new double[numInput];
		for(int i = 0; i < numInput; i++){
			outputProbabilities[i] = Double.MIN_VALUE;
		}
		
		int intAggregateWidth = (m_Rgba.cols())/numCols;
		int intAggregateHeight = (m_Rgba.rows())/numRows;///2
		int pos = 0;
		
		List<Mat> imgs = new ArrayList<Mat>();
		imgs.add(m_Rgba);
		
		Mat backproject = new Mat();
		
		MatOfFloat ranges = new MatOfFloat(0f, 256f,0f, 256f,0f, 256f);

List<int[]> subRegions = new ArrayList<int[]>();
int subRegionsIndex = 0;
for(int x = 0; x < (m_Rgba.cols()); x+=intAggregateWidth){	
	for(int y = 0; y < (m_Rgba.rows()) ; y+=intAggregateHeight){
		subRegions.add( new int[]{y, y+intAggregateHeight, x, x+intAggregateWidth} );
		subRegionsIndex =  subRegionsIndex+1;
	}
}
		//Calculate road values
		for(int i = 0; i < roadExamples.size(); i++){
			pos=0;
			
			backproject.create(m_Rgba.size(), m_Rgba.depth());
			Imgproc.calcBackProject(imgs, new MatOfInt(0,1,2), getModelHistogram(i,0), backproject, ranges, 100);
			
for(int j=0; j<subRegions.size();j++){
	Mat subRegion = backproject.submat(subRegions.get(j)[0], subRegions.get(j)[1], subRegions.get(j)[2], subRegions.get(j)[3]); 
	Scalar avg = Core.mean(subRegion);
	
	if(avg.val[0] > outputProbabilities[pos]){
		outputProbabilities[pos] = avg.val[0];
	}
	
	pos++;
}
//			for(int x = 0; x < (m_Rgba.cols()); x+=intAggregateWidth){	
//				for(int y = 0; y < (m_Rgba.rows()) ; y+=intAggregateHeight){
//					
//					Mat subRegion = backproject.submat(y, y+intAggregateHeight, x, x+intAggregateWidth); 
//					Scalar avg = Core.mean(subRegion);
//					
//					if(avg.val[0] > outputProbabilities[pos]){
//						outputProbabilities[pos] = avg.val[0];
//					}
//					
//					pos++;
//				}
//			}
		}
		
		return outputProbabilities;
	}
}
