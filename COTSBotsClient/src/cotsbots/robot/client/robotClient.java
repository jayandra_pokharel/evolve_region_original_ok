package cotsbots.robot.client;
import android.app.Activity;
import android.content.Context;
import android.util.Log;


import cotsbots.robot.data.RobotData;
import cotsbots.robot.data.RobotData.state;
import cotsbots.robot.robot.Robot;

import java.net.*;
import java.io.*;

import cotsbots.com.message.Message;
import cotsbots.graduate.robotcontroller.RobotController.SteeringType;



public class robotClient {
	
	
	
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private Socket socket;
	private String server;
	public boolean connected = false;
	public int ID = -1;
	private int port;
	public Robot myRobot;
	public RobotData myRobotData;
	
	public remoteReciever remote;
	
	
	
	public robotClient( String serv, int p, SteeringType drive, String ip, Activity c){
		server = serv;
		port = p;
		myRobotData = new RobotData();
		remote = new remoteReciever();
		myRobot = new Robot(ip,  drive, myRobotData, c);

	}
	
	public void start(){
		/*
		try{ 
			Log.e("Robo Client", "Connecting to " + server + " on " + port);
			InetAddress serverAddr = InetAddress.getByName(server);
			socket = new Socket(serverAddr, port);
			Log.e("Robo Client", "Connected");
		}catch(Exception e){
			Log.e("Robo Client", "Not Connected");
		}
		
		try{
			input = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());
		
			Log.e("Robo Client", "Streams Made");
			connected = true;
		}catch(Exception e){
			Log.e("Robo Client", "Streams not made");
		}
		if ( connected ){
			new ServerListener().start();
			new ServerUpdater().start();
		*/
			myRobot.start();
			remote.start();
			while(!remote.connected){
				//Log.e("RV", "Remote in loop " +i);
				//i++;
				;
			}
			new RemoteListener().start();
			Log.e("Robo Client", "Robot started");
			myRobot.myData.evoReady=true;
			myRobot.setupEvolution();
		//}
		
		
	}
	
	public void sendBest(double [] best){
		Message m = new Message();
		m.ID = myRobot.myData.ID;
		m.msgType = m.updateElite;
		m.data.evoData.elite = best;
		sendMessage(m);
	}
	
	public void sendMessage(Message m){
		try {
			output.writeObject(m);
		} catch (IOException e) {
			
		}
	}
	public void updateServer(){
		Message m = new Message();
		m.msgType = m.robotUpdate;

		m.ID 								= 	myRobot.myData.ID;
		m.data.currentState 				=	myRobot.myData.currentState;
		m.data.evoReady 					= 	myRobot.myData.evoReady;
		m.data.Conditions.maxGenerations 	= 	myRobot.myData.Conditions.maxGenerations;
		m.data.Conditions.distributed 		= 	myRobot.myData.Conditions.distributed;
		m.data.Conditions.numberOfDistBots 	= 	myRobot.myData.Conditions.numberOfDistBots;
		m.data.Conditions.sharedTestcases 	= 	myRobot.myData.Conditions.sharedTestcases;
		m.data.Conditions.startCondition 	= 	myRobot.myData.Conditions.startCondition;
		m.data.evoData.currentAverage 		= 	myRobot.myData.evoData.currentAverage;
		m.data.evoData.currentBest 			= 	myRobot.myData.evoData.currentBest;
		
		m.data.evoData.numGenerations 		= 	myRobot.myData.evoData.numGenerations;
		m.data.evoData.numTestCases			= 	myRobot.myData.evoData.numTestCases;
		
		m.data.evoData.currentGeneration 	= 	myRobot.myData.evoData.currentGeneration;
		m.data.evoData.averageFitByGeneration.clear();
		m.data.evoData.bestFitByGeneration.clear();
		for( int i = 0; i < myRobot.myData.evoData.averageFitByGeneration.size(); i++ ){
			m.data.evoData.averageFitByGeneration.add(myRobot.myData.evoData.averageFitByGeneration.get(i));
		}
		for( int i = 0; i < myRobot.myData.evoData.bestFitByGeneration.size(); i++ ){
			m.data.evoData.bestFitByGeneration.add(myRobot.myData.evoData.bestFitByGeneration.get(i));
		}
		m.data.evoData.left = myRobot.myData.evoData.left;
		m.data.evoData.right = myRobot.myData.evoData.right;
		m.data.evoData.forward = myRobot.myData.evoData.forward;
		
		m.data.evoData.numRecElites = myRobot.myData.evoData.numRecElites;
		
		sendMessage(m);
		
		
		
	}
	
	class ServerListener extends Thread{
		public void run(){
			while(true){
				
				try{
					Message m = (Message)input.readObject();
					Log.e("Robo Client", "Message Recieved "+ m.msgType);
					switch(m.msgType){
					case 0://new connection
						ID = m.ID;
						myRobot.myData.ID = m.ID;
						break;
					case 1: // movement command
						Log.e("Robo Client", "Drive Message Recieved");
							myRobot.drive(m.direction);
						break;
					case 2: // set Evolution Conditions
						Log.e("Robo Client", "EC Message Recieved");
						myRobot.myData.evoReady = true;
						myRobot.myData.Conditions = m.data.Conditions;
						myRobot.setupEvolution();
						break;
					case 3: // state change
						Log.e("Robo Client", "Change State Message Recieved");
						myRobot.changeState(m.data.currentState);
						break;
					case 4:
						break;
					case 5: //update
						Log.e("Robo Client", "Update Message Recieved");
						updateServer();
						break;
					case 6: // pause
						myRobot.pause();
						break;
					case 7: //evolve
						myRobot.startEvolution();
					case 8:
						break;
					case 9: // recieve elites
						Log.e("Robo Client", "Elites Message Recieved");
						myRobot.evolver.trainer.updateElites(m.data.evoData.elitePopulation);
						break;
					}
					
				}catch(Exception e){
					
				}
			}
		}
	}
	
	
	class RemoteListener extends Thread{
		public void run(){
			while(true){
				
				try{
					if (remote.ready()){
						char m = remote.read();
						Log.e("Robo Client", "Message Recieved");
						switch(m){
						case '0'://pause
							Log.e("Robo Client", "pause Recieved");
							myRobot.pause();
							break;
						case '1': // forward
							Log.e("Robo Client", "forward Recieved");
							myRobot.drive(1);
							break;
						
						case '3':// left
							Log.e("Robo Client", "left Recieved");
							myRobot.drive(3);
							break;
						case '4': // right
							Log.e("Robo Client", "right Recieved");
							myRobot.drive(4);
							break;
						
						case '6': // Teleoperate
							myRobot.changeState(state.TELEOPERATED);
							break;
						case '7': // Train Vision
							myRobot.changeState(state.VISIONTRAINING);
							break;
						case '8': // Train ANN
							myRobot.changeState(state.ANNTRAINING);
							break;
						case '9': //Autonomous
							myRobot.changeState(state.AUTONOMOUS);
							break;
						}
					}
					}catch(Exception e){
						
					}
				}
			
		}
	}
	
	
	
	class ServerUpdater extends Thread{
		public void run(){
			int lastGen = 0;
			while(true){
				if( myRobot.evolving ){
					if(myRobot.evolver.currentGeneration > lastGen){
						sendBest(myRobot.evolver.bestArray);
						lastGen = myRobot.evolver.currentGeneration;
					}
				}
			}
		}
	}
	
	
}

