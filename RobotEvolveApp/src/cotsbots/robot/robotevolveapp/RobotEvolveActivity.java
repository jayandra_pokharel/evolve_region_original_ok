package cotsbots.robot.robotevolveapp;


import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.OpenCVLoader;

import cotsbots.devicedescovery.BluetoothDiscoveryActivity;
import cotsbots.graduate.robotcontroller.RobotController.SteeringType;
import cotsbots.robot.client.robotClient;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;



public class RobotEvolveActivity extends Activity {
	public robotClient client;
	public String robotAddress;
	
	private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
		
		public void onManagerConnected(int status) {
		}};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		
		setContentView(R.layout.activity_robot_evolve);
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mOpenCVCallBack);
		
		Intent check = new Intent();
		check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(check, Activity.RESULT_OK);
		robotAddress = "00:11:05:09:03:39";		
//		Intent macIntent = new Intent(this, BluetoothDiscoveryActivity.class);
//		startActivityForResult(macIntent, BluetoothDiscoveryActivity.sf_REQUEST_CODE_BLUETOOTH);
	}

	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BluetoothDiscoveryActivity.sf_REQUEST_CODE_BLUETOOTH) {
			setContentView(R.layout.activity_robot_evolve);
			if (resultCode == RESULT_OK) {
				
//				robotAddress = data.getStringExtra(BluetoothDiscoveryActivity.sf_SELECTED_MAC_ADDRESS);
				
			}
		}
		
		
	}
	
	
	
	public void startRobot(View view){
		client = new robotClient("192.168.0.2", 10000, SteeringType.TRACKED, robotAddress, this);
		client.start();
		while(client.myRobot.myRobotView == null){
			;
		}
		setContentView(client.myRobot.myRobotView);
	}

}
