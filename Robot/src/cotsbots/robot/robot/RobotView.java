

package cotsbots.robot.robot;





import java.util.ArrayList;
import java.util.List;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;

import ui.lair.roadClassification.RoadClassifier;

import android.graphics.Canvas;
import android.app.Activity;



class RobotView extends ViewBase {

	

	private Mat m_Rgba = new Mat();
	private Mat overlay = new Mat();
	public RoadClassifier imageProcessor;
	public int col = 8, rows = 5;
	public boolean overlayOn = false;
	
	public RobotView(Activity context, RoadClassifier iProcessor) {
		super(context);
	imageProcessor = iProcessor;
	
	}

	

	

	public void pause() {
		this.m_IsActivityPaused = true;
		
	}



	@Override
	protected void processFrame(VideoCapture capture) {
		capture.retrieve(m_Rgba, Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGBA);
		/*
		if( overlayOn ){
			m_Rgba.copyTo(overlay);
			drawOverlay(col,rows,imageProcessor.calc(m_Rgba, col, rows));
			Core.addWeighted(m_Rgba, .4, overlay, .6, 0, m_Rgba);
		}else{
			drawbox();
		}
		*/
		//drawbox();
		
		Utils.matToBitmap(m_Rgba, mBmpCanvas);
		drawCameraToScreen();
		
	}
	
	private void drawOverlay(int numCols, int numRows, double[] outputProbabilities){
		int pos = 0;
		
		int intAggregateWidth = (m_Rgba.cols())/numCols;
		int intAggregateHeight = (m_Rgba.rows())/numRows;///2
		
		for(int x = 0; x < (m_Rgba.cols()); x+=intAggregateWidth){	
			for(int y = 0; y < (m_Rgba.rows()) ; y+=intAggregateHeight){
				List<MatOfPoint> points = new ArrayList<MatOfPoint>();
				
				points.add( new MatOfPoint( 
											new Point( (double)x, (double)y ),
											new Point( (double)x+intAggregateWidth, (double)y ),
											new Point( (double)x+intAggregateWidth, (double)y+intAggregateHeight ),
											new Point( (double)x, (double)y+intAggregateHeight))
											);
				colorANNInputRegion(overlay, points, outputProbabilities[pos]);
				pos++;
			}
		}
	}
	
public void drawbox(){
	int startx = 315, endx = 365, starty = 400, endy = 450;
		double[] color = {255,0,0};
		double[] color2 = {255,0,0,0};
		for ( int x = startx; x < endx; x++){
			for( int y = starty; y < endy; y++ ){
				if( x == startx || y == starty || x == endx-1|| y == endy-1){
					
					if (m_Rgba.channels() == 3){
						m_Rgba.put(y, x, color);
					}
					else{
						m_Rgba.put(y, x, color2);
					}
				}
			}
		}
		
	}
	
	
	private void colorANNInputRegion(Mat overlay, List<MatOfPoint> points, double prob){
		
		Scalar veryHighColor = new Scalar(0, 255, 0,0);
		Scalar highColor = new Scalar(153, 255, 0,0);
		Scalar medColor = new Scalar(255 , 255 , 50,0);
		Scalar lowColor = new Scalar(255 , 153, 50,0);
		Scalar veryLowColor = new Scalar(255 , 0, 0,0);
		
		Scalar neg1 = new Scalar(213 , 19 , 36,0);
		Scalar neg2 = new Scalar(230 , 10, 220,0);
		Scalar neg3 = new Scalar(140 , 7, 240,0);
		
		if (prob > .5){
			Core.fillPoly(overlay, points, veryHighColor);
		}
		else if(prob > 0){
			Core.fillPoly(overlay, points, veryLowColor);
		}
		else {
			Core.fillPoly(overlay, points, veryLowColor);
		}
	}
	
	
	
	public Mat getView(){
		return m_Rgba;
	}
	
	public void drawCameraToScreen(){
		
		if (mBmpCanvas != null) {
			Canvas canvas = m_Holder.lockCanvas();
			
			if (canvas != null) {
				// Clears screen for redrawing FPS
				canvas.drawColor(android.graphics.Color.BLACK);

				
				 // For drawing the camera image to screen ///////
				 canvas.drawBitmap(mBmpCanvas, (canvas.getWidth() -
				 mBmpCanvas.getWidth()) / 2, (canvas.getHeight() -
				 mBmpCanvas.getHeight()) / 2, null);
				
		
			
				m_Holder.unlockCanvasAndPost(canvas);
			}
		}
	}

	
	
	
}
