The Robot Server Project

The projects below were developed in the Eclipse IDE.

COTSBotsClient (Android Project): client that runs on the robot that
connects to the server to communicate data and commands

COTSBotsServer (Java Project): runs on the computer to enable data
and command sharing.   Uses a library called Windows Builder Pro.

CommunicationMessage (Java Project): An object.  Describes data
structure that holds the data for communication between bots.  Used by
COTSBotsServer and COTSBotsClient.

CotsBotsBluetoothDiscovery (Android Project): Module that allows us to
find the bluetooth mac address.

CotsBotsRobotController (Android Project): Module to connect phone to
bluetooth on Arduino.  Sends commands to Arduino.  {To Do: send
commands back from Arduino esp. to handle disconnects.}

ImageLibrary (DELETE)

ImagingLibrary-master (Android Project) : wraps openCV and handles vision tasks

OpenCV-2.3.1 (DELETE)
OpenCV-2.4.6-android-sdk : Used by ImagingLibrary-master.

Remote (Android Project): creates GUI for the phone controller.

Robot (Android Project): Robotview class.  Evolution and NN code.
Robot logic.  Uses CotsBotsRobotController to send messages to robot
controler.  Contains RobotData.

RobotData (Java Project): data object for the robot and evolution
setup and evolution data

RobotEvolveApp (Android Project): activity that starts Robot project
Handles communciation between robot and client.  Drives the evolution
and learning using Robot and COTSBotsClient

networkGenAlgorithm (DELETE)

neuroph (NN Library) : imported NN library

